# debian client name
class mysql::client::debian inherits mysql::client::base {
  if versioncmp($::operatingsystemmajrelease,'9') >= 0 {
    Package[mysql]{
      name => 'mariadb-client'
    }
  } else {
    Package['mysql'] {
      name => 'mysql-client',
    }
  }
}
