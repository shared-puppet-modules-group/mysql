# debian ruby client
class mysql::client::ruby::debian {
  if versioncmp($::operatingsystemmajrelease,'7') >= 0 {
    $package = 'ruby-mysql'
  } else {
    $package = 'libmysql-ruby'
  }
  ensure_packages([ $package ])
}
